# Unsubscribe Email Header

This standard has been cancelled in favor of an existing standard. See the equivalent, complete standard in section 3.2 of [RFC 2369](https://tools.ietf.org/html/rfc2369).
